﻿using Blockchain.Repositories;

namespace Blockchain.Data
{
    public interface IDbInitializer
    {
        void SeedData(AppDbContext context, ITransactionsRepository repository);
    }
}