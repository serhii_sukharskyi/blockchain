﻿using System;
using System.Linq;
using Blockchain.Models;
using Blockchain.Repositories;
using Microsoft.EntityFrameworkCore;

namespace Blockchain.Data
{
    public class DbInitializer : IDbInitializer
    {
        public void SeedData(AppDbContext context, ITransactionsRepository repository)
        {
            context.Database.Migrate();
            AddTransactions(repository);
        }

        private static void AddTransactions(ITransactionsRepository repository)
        {
            if (repository.GetAll().Any()) return;

            repository.Add(new Transaction
            {
                Date = new DateTime(2017, 01, 01),
                Description = "Description",
                Requestor = "Requestor",
                Location = "Toronto",
                TransactionCode = "ASDFGH-1234544",
                Status = "Open"
            });
            repository.Add(new Transaction
            {
                Date = new DateTime(2017, 01, 02),
                Description = "Some desc",
                Requestor = "Oleksandra Bosa",
                Location = "Washinghton",
                TransactionCode = "ASDFGH-1234567",
                Status = "Approved"
            });
            repository.Add(new Transaction
            {
                Date = new DateTime(2017, 01, 03),
                Description = "Interesting desc",
                Requestor = "Goverment",
                Location = "Toronto",
                TransactionCode = "ASDFGH-1234577",
                Status = "Pending"
            });
            repository.Add(new Transaction
            {
                Date = new DateTime(2017, 01, 03),
                Description = "Unique desc",
                Requestor = "Sergiy Sukharskyi",
                Location = "New York",
                TransactionCode = "ASDFGH-1234554",
                Status = "Open"
            });
            repository.Add(new Transaction
            {
                Date = new DateTime(2017, 01, 04),
                Description = "Something",
                Requestor = "Alla Sidko",
                Location = "Ottawa",
                TransactionCode = "ASDFGH-1234568",
                Status = "Closed"
            });
        }
    }
}
