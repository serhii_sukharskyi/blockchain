var service = 'http://localhost:5000';

class Transaction {
    constructor(id, date, requestor, location, status, transactionCode, description, requestorLogo) {
        this.id = id;
        this.date = date;
        this.requestor = requestor;
        this.location = location;
        this.status = status;
        this.transactionCode = transactionCode;
        this.description = description;
        this.requestorLogo = requestorLogo;
    }
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function toJSONString( form ) {
    var obj = {};
    var elements = form.serializeArray();
    for( var i = 0; i < elements.length; ++i ) {
        var element = elements[i];
        var name = element.name;
        var value = element.value;

        if( name ) {
            obj[ name ] = value;
        }
    }

    return JSON.stringify( obj );
}

function formatDate(date) {
    var hours = date.getHours();
    hours = hours > 9 ? '' + hours : '0' + hours;

    var minutes = date.getMinutes();
    minutes = minutes > 9 ? '' + minutes : '0' + minutes;

    return date.toISOString().split('T')[0] + ' ' + hours + ':' + minutes;
}

function getTransactions()
{
    jQuery.support.cors = true;

    $.ajax({
        type: "GET",
        url: service + '/api/transactions',
        data: "{}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        cache: false,
        success: function (data) {
            var trHTML = '';

            $.each(data, function (i, obj) {
                var item = $.extend(obj);
                trHTML += '<tr id="tr-'+item.id+'"><td>' + item.id + '</td><td>' + formatDate(new Date(item.date)) + '</td><td>' + item.requestor +
                    '</td><td>' + item.location + '</td><td>' + item.status +'</td>' + 
                    '<td><button id="'+item.id+'" onclick="toDetailsPage(this.id)" class="btn btn-info btn-sm" type="button"><span class="glyphicon glyphicon-eye-open"></span></button></td>'+                    
                    '<td><button id="'+item.id+'" onclick="deleteTransaction(this.id)" class="btn btn-danger btn-sm" type="button"><span class="glyphicon glyphicon-trash"></span></button></td>' +
                    '</tr>';
            });
            $('#transTable').append(trHTML);
            
        },
        error: function (msg) {
            showAlert("#main_alert_placeholder", msg.responseText);
        }
    });
}

function getTransactionDetails(id)
{
    $.ajax({
        type:'GET',
        url: service + '/api/transactions/' + id,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {

            var transaction = $.extend(data);
            $("#det-req-value").text(transaction.requestor);
            $("#det-date-value").text(formatDate(new Date(transaction.date)));
            $("#det-loc-value").text(transaction.location);
            $("#det-status-value").text(transaction.status);
            $("#det-tcode-value").text(transaction.transactionCode);
            $("#det-desc-value").text(transaction.description);
            $("#det-reqlogo-value").attr('src', transaction.requestorLogo); 
        },
        error: function (msg) {
            showAlert("#alert_placeholder", msg.responseText);
        }
    }) 
}

function deleteTransaction(id){
    $.ajax({
        url: service + '/api/transactions/' + id,
        type: 'DELETE',
        success: function (result) {
            $('#main_alert_placeholder').html(
                '<div  id="snoAlertBox" class="alert alert-warning alert-dismissible" role="alert">' +
                '<button type="button" class="close" ' +
                'data-dismiss="alert" aria-hidden="true">' +
                '&times;' +
                '</button><p>Successfully deleted!</p></div>');
            $("#tr-" + id).remove();
            $("#snoAlertBox").fadeIn();
            window.setTimeout(function () {
                $("#snoAlertBox").fadeOut(300)
            }, 3000);
        },
        error: function (msg) {
            showAlert("#main_alert_placeholder", msg.responseText);
        }
    });
}

function createTransaction(data){
    console.log(data);
    
    $.ajax({
        url: service + '/api/transactions/',
        type: 'POST',
        data: data,
        dataType: "json",
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            toHomePage();
        },
        error: function (msg) {
            showAlert("#alert_placeholder", msg.responseText);
        }
    });
}

function toDetailsPage(id)
{
    location.href=service + '/details.html?id=' + id;
}

function toHomePage()
{
    location.href = service;
}

function showAlert(whereToPut, msg)
{
    $(whereToPut).html(
        '<div class="alert alert-warning alert-dismissible" role="alert">' +
        '<button type="button" class="close" ' +
        'data-dismiss="alert" aria-hidden="true">' +
        '&times;' +
        '</button> <strong>Oops...   </strong><p>' + msg + '</p></div>');
}