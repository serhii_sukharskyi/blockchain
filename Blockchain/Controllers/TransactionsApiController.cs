﻿using System;
using System.IO;
using System.Net.Http.Headers;
using Blockchain.Models;
using Blockchain.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace Blockchain.Controllers
{
    [Route("api/transactions")]
    public class TransactionsApiController : Controller
    {
        private readonly ITransactionsRepository _transactionsRepository;
        private readonly UploadedFilesSettings _filesSettings;

        public TransactionsApiController(ITransactionsRepository repo, IOptions<UploadedFilesSettings> filesSettings)
        {
            _transactionsRepository = repo;
            _filesSettings = filesSettings.Value;
        }

        // GET: api/transactions
        [HttpGet]
        public IActionResult Get()
        {
            return new ObjectResult(_transactionsRepository.GetAll());
        }

        // GET api/transactions/5
        [HttpGet("{id}", Name="GetTransaction")]
        public IActionResult Get(int id)
        {
            if (id <= 0)
                return BadRequest("Wrong id");

            Transaction transaction = null;
            try
            {
                transaction = _transactionsRepository.Get(id);

                if (transaction == null)
                    return NotFound();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return BadRequest(e.Message);
            }
            if (String.IsNullOrEmpty(transaction.RequestorLogo))
                transaction.RequestorLogo = "images/upload/images.png";

            return new ObjectResult(transaction);
        }

        // POST api/transactions
        [HttpPost]
        [Consumes("multipart/form-data", "application/json", "application/x-www-form-urlencoded", "application/form-data")]
        public IActionResult Post([FromForm]Transaction entity)
        {
            if (entity == null)
                return BadRequest("Object reference not set to an instance of an object");
            
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                //Get the first file submitted by form 
                var file = HttpContext.Request.Form.Files[0];

                if (file != null)
                {
                    // Get informaion about file, get filename and remove quotes symbols
                    var originalFilename = ContentDispositionHeaderValue
                        .Parse(file.ContentDisposition)
                        .FileName
                        .Trim('"');

                    if (!string.IsNullOrEmpty(originalFilename))
                    {
                        string filePath = UploadImage(file, originalFilename);
                        entity.RequestorLogo = !string.IsNullOrEmpty(filePath) ? filePath : "";
                    }
                }

                _transactionsRepository.Add(entity);
            }
            catch (DbUpdateException e)
            {
                Console.WriteLine(e.InnerException.Message);
                ModelState.AddModelError("0", e.InnerException.Message);
                return BadRequest(ModelState);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                return BadRequest(e.Message);
            }
            return CreatedAtRoute("GetTransaction", new { id = entity.Id }, entity);
        }

        // PUT api/transactions/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody]Transaction entity)
        {
            if (entity == null || entity.Id != id)
                return BadRequest("Object reference not set to an instance of an object");
            
            if (!ModelState.IsValid)
                return BadRequest(ModelState);
            
            var oldEntity = _transactionsRepository.Get(id);

            if (oldEntity == null)
            {
                try
                {
                    HttpContext.Response.StatusCode = 404;
                    return Json(new { message = $"{entity.Id} not found" });
                }
                catch (Exception)
                {
                    return NotFound();
                }
            }
            
            _transactionsRepository.Edit(entity);

            return new NoContentResult();
        }

        // DELETE api/transactions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var entity = _transactionsRepository.Get(id);
            if (entity == null)
            {
                try
                {
                    HttpContext.Response.StatusCode = 404;
                    return Json(new { message = $"{entity.Id} not found" });
                }
                catch (Exception)
                {
                    return NotFound();
                }
            }
            var deleteSuccessful = _transactionsRepository.Delete(entity);

            if (!deleteSuccessful)
                return BadRequest($"Error during {entity.Id} deletion");
            
            return new NoContentResult();
        }


        private string UploadImage(IFormFile file, string originalFilename)
        {
            try
            {
                var physicalUrl = _filesSettings.PhysicalPath.Replace("\\", "/");
                var pathToFile = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", physicalUrl);

                if(!Directory.Exists(pathToFile))
                {
                    Directory.CreateDirectory(pathToFile);
                }

                var path = NormalizeFilename(Path.Combine(pathToFile, originalFilename));

                using (FileStream fs = System.IO.File.Create(path))
                {
                    file.CopyTo(fs);
                    fs.Flush();
                }

                return Path.Combine(physicalUrl, Path.GetFileName(path));
            }
            catch (Exception ex)
            {
                Console.WriteLine("###" + ex.Message + "###");
                return null;
            }
        }

        private string NormalizeFilename(string originalFilePath)
        {
            var fullPath = originalFilePath.Replace(" ", "_");

            int count = 1;

            string fileNameOnly = Path.GetFileNameWithoutExtension(fullPath);
            string extension = Path.GetExtension(fullPath);
            string path = Path.GetDirectoryName(fullPath);
            string newFullPath = fullPath;

            while (System.IO.File.Exists(newFullPath))
            {
                string tempFileName = string.Format($"{fileNameOnly}({count++})");
                newFullPath = Path.Combine(path, tempFileName + extension);
            }
            return newFullPath;
        }
    }
}
