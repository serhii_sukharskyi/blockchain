﻿namespace Blockchain.Models
{
    public class UploadedFilesSettings
    {
        public string PhysicalPath { get; set; }
    }
}
