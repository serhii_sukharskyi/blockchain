﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Blockchain.Models
{
    public class Transaction
    {
        [Key]
        public int Id { get; set; }

        public DateTime Date { get; set; }

        [Required]
        public string Requestor { get; set; }

        [Required]
        public string Location { get; set; }

        [Required]
        public string Status { get; set; }

        [Required]
        public string TransactionCode { get; set; }

        [Required]
        public string Description { get; set; }

        public string RequestorLogo { get; set; }
    }
}