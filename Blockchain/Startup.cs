﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Blockchain.Data;
using Blockchain.Repositories;
using Microsoft.EntityFrameworkCore;
using Blockchain.Models;

namespace Blockchain
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();

            services.AddCors(options => options.AddPolicy("AllowAll", p => p.AllowAnyOrigin()
               .AllowAnyMethod()
               .AllowAnyHeader()));

            services.AddOptions();
            services.Configure<UploadedFilesSettings>(Configuration.GetSection("UploadedFilesSettings"));

            // Scoped
            services.AddScoped<ITransactionsRepository, TransactionsRepository>();

            // Transient
            services.AddTransient<IDbInitializer, DbInitializer>();

            services.AddDbContext<AppDbContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("SqlConnectionString")));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseCors("AllowAll");

            app.UseMvc();
            
            InitializeDatabase(app);
        }

        public virtual void InitializeDatabase(IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<AppDbContext>();
                var repository = serviceScope.ServiceProvider.GetService<ITransactionsRepository>();
                var databaseInitializer = app.ApplicationServices.GetService<IDbInitializer>();

                databaseInitializer.SeedData(context, repository);
            }
        }
    }
}
