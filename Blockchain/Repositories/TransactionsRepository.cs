﻿using System;
using System.Collections.Generic;
using System.Linq;
using Blockchain.Data;
using Blockchain.Models;
using Microsoft.EntityFrameworkCore;

namespace Blockchain.Repositories
{
    public interface ITransactionsRepository
    {
        List<Transaction> GetAll();
        Transaction Get(int id);
        void Add(Transaction item);
        Transaction Edit(Transaction item);
        bool Delete(Transaction item);
    }

    public class TransactionsRepository : ITransactionsRepository
    {
        private readonly AppDbContext _context;

        public TransactionsRepository(AppDbContext context)
        {
            _context = context;
        }

        public List<Transaction> GetAll() => _context.Transactions.AsNoTracking().ToList();

        public Transaction Get(int id) => _context.Transactions.AsNoTracking()
            .SingleOrDefault(item => (int)item.GetType().GetProperty("Id").GetValue(item) == id);
        
        public void Add(Transaction item)
        {
            item.Date = DateTime.Now;
            _context.Transactions.Add(item);
            _context.SaveChanges();
            _context.Entry(item).State = EntityState.Detached;
        }

        public Transaction Edit(Transaction item)
        {
            _context.Transactions.Update(item);
            _context.SaveChanges();
            _context.Entry(item).State = EntityState.Detached;

            return item;
        }

        public bool Delete(Transaction item)
        {
            _context.Transactions.Remove(item);
            _context.SaveChanges();
            _context.Entry(item).State = EntityState.Detached;

            return true;
        }
    }
}